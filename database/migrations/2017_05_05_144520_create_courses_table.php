<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         * weekday =>
         *
         *
         * @author tfhnia
         * @return void
         */
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id');
            $table->string('name');
            $table->string('weekday');
            $table->string('period');
            $table->string('from_week');
            $table->string('end_week');
            $table->tinyInteger('credit');
            $table->string('teacher');
            $table->string('place');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
